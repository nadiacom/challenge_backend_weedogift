package backend_challenge.security;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    public boolean userHasAccess(int userId) {
        int jwtUserId = Integer.parseInt((String) (((Jwt) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getClaims()
                .get("user_id")));
        return userId == jwtUserId;
    }
}
