package backend_challenge.repository;

import backend_challenge.domain.Company;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;

/**
 * This is an in memory db that is populated with some companies at each run
 */
@Repository
public class CompanyRepository {

    private final Map<Integer, Company> companies;

    public CompanyRepository() {
        this.companies = Map.of(1, new Company(1, "Wedoogift", 1000),
                2, new Company(2, "Wedoofood", 3000));
    }

    public Optional<Company> findById(int id) {
        return Optional.ofNullable(companies.get(id));
    }
}
