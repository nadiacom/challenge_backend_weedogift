package backend_challenge.repository;

import backend_challenge.domain.Balance;
import backend_challenge.domain.User;
import backend_challenge.domain.WalletType;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * This is an in memory db that is populated with some users at each run
 */
@Repository
public class UserRepository {

    private final Map<Integer, User> users;

    public UserRepository() {
        this.users = Map.of(1, new User(1, new HashMap<>(Map.of(WalletType.GIFT, new Balance(50)))),
                2, new User(2, new HashMap<>(Map.of(
                        WalletType.GIFT, new Balance(50),
                        WalletType.FOOD, new Balance(10))
                )));
    }

    public Optional<User> findById(int id) {
        return Optional.ofNullable(users.get(id));
    }
}
