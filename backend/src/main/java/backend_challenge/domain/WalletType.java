package backend_challenge.domain;

import backend_challenge.domain.exceptions.NotFoundException;
import com.fasterxml.jackson.annotation.JsonValue;

public enum WalletType {

    GIFT(1),
    FOOD(2),
    ;
    private final int id;

    WalletType(int id) {
        this.id = id;
    }

    @JsonValue
    public int getId() {
        return id;
    }

    public static WalletType resolveWalletType(int walletId) throws NotFoundException {
        for (WalletType walletType : values()) {
            if (walletType.id == walletId) {
                return walletType;
            }
        }
        throw NotFoundException.walletTypeNotFound(walletId);
    }
}
