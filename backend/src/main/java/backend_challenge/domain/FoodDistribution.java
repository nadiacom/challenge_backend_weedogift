package backend_challenge.domain;

import java.time.Clock;
import java.time.LocalDate;

public class FoodDistribution extends Distribution {

    public FoodDistribution(int id, int amount, Clock clock) {
        super(id, amount);
        this.wallet = WalletType.FOOD;
        this.startDate = LocalDate.now(clock);
        this.endDate = this.startDate.withDayOfMonth(this.startDate.lengthOfMonth()).plusDays(365);
    }

}
