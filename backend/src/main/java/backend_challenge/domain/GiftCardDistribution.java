package backend_challenge.domain;

import java.time.Clock;
import java.time.LocalDate;

public class GiftCardDistribution extends Distribution {

    public GiftCardDistribution(int id, int amount, Clock clock) {
        super(id, amount);
        this.wallet = WalletType.GIFT;
        this.startDate = LocalDate.now(clock);
        // README says "The distribution of gift cards has a lifespan of 365 days", not one year
        // expected-output seems to be one day earlier though
        // This implementation might be correct since isExpired method use strict isAfter method
        this.endDate = this.startDate.plusDays(365);
    }
}
