package backend_challenge.domain;

import backend_challenge.domain.exceptions.InsufficientCompanyBalanceException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Company {
    private final int id;
    private final String name;
    private int balance;

    @JsonCreator
    public Company(@JsonProperty("id")  int id, @JsonProperty("name") String name, @JsonProperty("balance") int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public int getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }

    /**
     * This method add distribution to user
     *
     * It subtracts distribution's amount from company's balance
     * @param user
     * @param distribution
     * @return created distribution filled with userId and companyId
     * @throws InsufficientCompanyBalanceException
     */
    public Distribution distribute(User user, Distribution distribution) throws InsufficientCompanyBalanceException {
        if (this.balance < distribution.getAmount()) {
            throw new InsufficientCompanyBalanceException(this, distribution.getAmount());
        }
        this.balance -= distribution.getAmount();
        // This is only used to match with expected-output.json
        distribution.linkToCompanyAndUser(this.id, user.getId());
        user.receiveDistribution(distribution);
        return distribution;
    }
}
