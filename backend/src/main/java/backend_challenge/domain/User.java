package backend_challenge.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class User {

    private final int id;
    private final Map<WalletType, Balance> walletBalances;

    public User(int id, Map<WalletType, Balance> walletBalances) {
        this.id = id;
        this.walletBalances = walletBalances;
    }

    /**
     * This method adds distributions by wallet type
     *
     * @param distribution
     */
    void receiveDistribution(Distribution distribution) {
        if (this.walletBalances.containsKey(distribution.getWallet())) {
            this.walletBalances.get(distribution.getWallet()).addDistribution(distribution);
        } else {
            this.walletBalances.put(distribution.getWallet(),
                    new Balance(0, new ArrayList<>(List.of(distribution))));
        }
    }

    public int getId() {
        return id;
    }

    public Map<WalletType, Balance> getWalletBalances() {
        return walletBalances;
    }
}
