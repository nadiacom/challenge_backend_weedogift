package backend_challenge.domain;

import java.time.Clock;
import java.util.ArrayList;
import java.util.List;

public class Balance {

    private final int initialAmount;
    private List<Distribution> distributions;

    public Balance(int initialAmount) {
        this.initialAmount = initialAmount;
        this.distributions = new ArrayList<>();
    }

    public Balance(int initialAmount, List<Distribution> distributions) {
        this.initialAmount = initialAmount;
        this.distributions = distributions;
    }

    public void addDistribution(Distribution distribution) {
        this.distributions.add(distribution);
    }

    /**
     * This method compute and return current balance amount
     *
     * It doesn't sum amount of expired cards
     * It sums initial user's balance amount
     * @return
     */
    public int getBalanceAmount() {
        return this.getDistributions().stream()
                .filter(distribution -> !distribution.isExpired(Clock.systemDefaultZone()))
                .map(Distribution::getAmount)
                .reduce(this.getInitialAmount(), Integer::sum);
    }

    public List<Distribution> getDistributions() {
        return distributions;
    }

    public int getInitialAmount() {
        return initialAmount;
    }
}
