package backend_challenge.domain.exceptions;

import backend_challenge.domain.Company;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class InsufficientCompanyBalanceException extends Exception {

    public InsufficientCompanyBalanceException(Company company, int amount) {
        super(
                "Company with id " + company.getId() + " has balance of " + company.getBalance() + ". Cannot distribute " + amount
        );
    }
}
