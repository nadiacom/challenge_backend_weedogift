package backend_challenge.domain.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends Exception {

    public NotFoundException(String message) {
        super(message);
    }

    public static NotFoundException userNotFound(int id) {
        return new NotFoundException("User with id " + id + " not found.");
    }

    public static NotFoundException companyNotFound(int id) {
        return new NotFoundException("Company with id " + id + " not found.");
    }

    public static NotFoundException walletTypeNotFound(int id) {
        return new NotFoundException("Wallet with id " + id + " not found.");
    }
}
