package backend_challenge.domain;

import backend_challenge.domain.exceptions.NotFoundException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import java.time.Clock;
import java.time.LocalDate;
import java.util.Random;

@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
public abstract class Distribution {
    private final int id;
    private final int amount;
    WalletType wallet;
    LocalDate startDate;
    LocalDate endDate;
    private Integer companyId;
    private Integer userId;

    public Distribution(int id, int amount) {
        this.id = id;
        this.amount = amount;
    }

    public boolean isExpired(Clock clock) {
        return LocalDate.now(clock).isAfter(this.endDate);
    }

    /**
     * This method is used to have companyId and userId in json output.
     *
     * @param companyId
     * @param userId
     */
    public void linkToCompanyAndUser(int companyId, int userId) {
        this.companyId = companyId;
        this.userId = userId;
    }

    public static Distribution createFrom(int walletId, int amount, Clock clock) throws NotFoundException {
        WalletType walletType = WalletType.resolveWalletType(walletId);
        Distribution distribution;
        switch (walletType) {
            // We don't have a db here, but distribution's id could have been automatically generated at insert
            case GIFT:
                distribution = new GiftCardDistribution(new Random().nextInt(), amount, clock);
                break;
            case FOOD:
                distribution = new FoodDistribution(new Random().nextInt(), amount, clock);
                break;
            default:
                throw new IllegalArgumentException();
        }
        return distribution;
    }

    public int getId() {
        return id;
    }

    public int getAmount() {
        return amount;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public Integer getCompanyId() {
        return companyId;
    }

    public Integer getUserId() {
        return userId;
    }

    @JsonIgnore
    public WalletType getWallet() {
        return wallet;
    }

    public int getWalletId() {
        return wallet.getId();
    }
}
