package backend_challenge.controller;

import backend_challenge.domain.Distribution;
import backend_challenge.domain.User;
import backend_challenge.domain.exceptions.InsufficientCompanyBalanceException;
import backend_challenge.domain.exceptions.NotFoundException;
import backend_challenge.repository.CompanyRepository;
import backend_challenge.repository.UserRepository;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.Clock;

@RestController
@RequestMapping(path = "/companies", produces = MediaType.APPLICATION_JSON_VALUE)
public class CompanyController {

    private final CompanyRepository companyRepository;
    private final UserRepository userRepository;
    private final Clock clock;

    public CompanyController(CompanyRepository companyRepository, UserRepository userRepository, Clock clock) {
        this.companyRepository = companyRepository;
        this.userRepository = userRepository;
        this.clock = clock;
    }

    @PostMapping("/{companyId}/users/{userId}/distributions")
    @ResponseStatus(HttpStatus.CREATED)
    public Distribution distribute(@PathVariable int companyId, @PathVariable int userId, @RequestBody DistributionDTO distributionDTO) throws NotFoundException, InsufficientCompanyBalanceException {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> NotFoundException.userNotFound(userId));

        return companyRepository.findById(companyId)
                .orElseThrow(() -> NotFoundException.companyNotFound(companyId))
                .distribute(user, Distribution.createFrom(distributionDTO.getWalletId(), distributionDTO.getAmount(), clock));
    }

    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class DistributionDTO {
        private final int walletId;
        private final int amount;

        public DistributionDTO(int walletId, int amount) {
            this.walletId = walletId;
            this.amount = amount;
        }

        public int getWalletId() {
            return walletId;
        }

        public int getAmount() {
            return amount;
        }
    }
}
