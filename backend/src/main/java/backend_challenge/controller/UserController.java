package backend_challenge.controller;

import backend_challenge.domain.User;
import backend_challenge.domain.exceptions.NotFoundException;
import backend_challenge.repository.UserRepository;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/{id}/balances")
    @PreAuthorize("@authenticationService.userHasAccess(#id)")
    public List<UserBalanceDTO> getUserBalance(@PathVariable int id) throws NotFoundException {
        return userRepository.findById(id).map(this::getBalance)
                .orElseThrow(() -> NotFoundException.userNotFound(id));
    }

    public List<UserBalanceDTO> getBalance(User user) {
        return user.getWalletBalances().keySet().stream()
                .map(walletType -> new UserBalanceDTO(walletType.getId(),
                        user.getWalletBalances().get(walletType).getBalanceAmount()))
                .sorted(Comparator.comparingInt(UserBalanceDTO::getWalletId))
                .collect(Collectors.toList());
    }

    @JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
    public static class UserBalanceDTO {
        private final int walletId;
        private final int amount;

        public UserBalanceDTO(int walletId, int amount) {
            this.walletId = walletId;
            this.amount = amount;
        }

        public int getWalletId() {
            return walletId;
        }

        public int getAmount() {
            return amount;
        }
    }
}
