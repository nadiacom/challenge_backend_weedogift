package backend_challenge.controller;

import backend_challenge.domain.Balance;
import backend_challenge.domain.User;
import backend_challenge.domain.WalletType;
import backend_challenge.repository.UserRepository;
import backend_challenge.security.AuthenticationService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIT {

    @Autowired
    private MockMvc mvc;

    @MockBean
    UserRepository userRepository;
    @MockBean
    AuthenticationService authenticationService;

    @Test
    @WithMockUser
    void should_return_user_balance() throws Exception {
        Map<WalletType, Balance> balances = Map.of(WalletType.GIFT, new Balance(50),
                WalletType.FOOD, new Balance(450));
        User user = new User(1, balances);
        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(authenticationService.userHasAccess(1)).thenReturn(true);

        mvc.perform(get("/users/1/balances")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].wallet_id", is(1)))
                .andExpect(jsonPath("$[0].amount", is(50)))
                .andExpect(jsonPath("$[1].wallet_id", is(2)))
                .andExpect(jsonPath("$[1].amount", is(450)));
    }

    @Test
    @WithMockUser
    void should_return_user_balance_when_user_not_authorized() throws Exception {
        when(authenticationService.userHasAccess(1)).thenReturn(false);

        mvc.perform(get("/users/1/balances")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }
}
