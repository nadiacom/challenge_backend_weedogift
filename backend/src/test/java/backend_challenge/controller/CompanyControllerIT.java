package backend_challenge.controller;

import backend_challenge.domain.Balance;
import backend_challenge.domain.Company;
import backend_challenge.domain.User;
import backend_challenge.domain.WalletType;
import backend_challenge.repository.CompanyRepository;
import backend_challenge.repository.UserRepository;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class CompanyControllerIT {

    @Autowired
    private MockMvc mvc;

    @MockBean
    UserRepository userRepository;
    @MockBean
    CompanyRepository companyRepository;

    @Test
    @WithMockUser
    void should_distribute_from_company_to_user() throws Exception {
        Map<WalletType, Balance> balances = Map.of(WalletType.GIFT, new Balance(50),
                WalletType.FOOD, new Balance(450));
        User user = new User(1, balances);
        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(companyRepository.findById(2)).thenReturn(Optional.of(new Company(2, "Weedofood", 50)));

        mvc.perform(MockMvcRequestBuilders
                        .post("/companies/2/users/1/distributions")
                        .content(new JSONObject()
                                .put("wallet_id", 1)
                                .put("amount", 50)
                                .toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("amount", is(50)))
                .andExpect(MockMvcResultMatchers.jsonPath("company_id", is(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("user_id", is(1)));
    }

    @Test
    @WithMockUser
    void should_return_forbidden_status_code_when_company_balance_is_not_sufficient() throws Exception {
        Map<WalletType, Balance> balances = Map.of(WalletType.GIFT, new Balance(50),
                WalletType.FOOD, new Balance(450));
        User user = new User(1, balances);
        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(companyRepository.findById(2)).thenReturn(Optional.of(new Company(2, "Weedofood", 50)));

        mvc.perform(MockMvcRequestBuilders
                        .post("/companies/2/users/1/distributions")
                        .content(new JSONObject()
                                .put("wallet_id", 1)
                                .put("amount", 100)
                                .toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());
    }

    @Test
    @WithMockUser
    void should_return_not_found_status_code_when_company_id_does_not_exist() throws Exception {
        Map<WalletType, Balance> balances = Map.of(WalletType.GIFT, new Balance(50),
                WalletType.FOOD, new Balance(450));
        User user = new User(1, balances);
        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(companyRepository.findById(2)).thenReturn(Optional.empty());

        mvc.perform(MockMvcRequestBuilders
                        .post("/companies/2/users/1/distributions")
                        .content(new JSONObject()
                                .put("wallet_id", 1)
                                .put("amount", 100)
                                .toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser
    void should_return_not_found_status_code_when_wallet_type_does_not_exist() throws Exception {
        Map<WalletType, Balance> balances = Map.of(WalletType.GIFT, new Balance(50),
                WalletType.FOOD, new Balance(450));
        User user = new User(1, balances);
        when(userRepository.findById(1)).thenReturn(Optional.of(user));
        when(companyRepository.findById(2)).thenReturn(Optional.empty());

        mvc.perform(MockMvcRequestBuilders
                        .post("/companies/2/users/1/distributions")
                        .content(new JSONObject()
                                .put("wallet_id", 3)
                                .put("amount", 100)
                                .toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_401_status_code_when_user_is_not_authorized() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                        .post("/companies/2/users/1/distributions")
                        .content(new JSONObject()
                                .put("wallet_id", 3)
                                .put("amount", 100)
                                .toString())
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }
}
