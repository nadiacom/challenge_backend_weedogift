package backend_challenge.domain;

import backend_challenge.domain.exceptions.InsufficientCompanyBalanceException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Clock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

public class CompanyTest {
    private Company company;
    private User user;

    @BeforeEach
    void setup() {
        company = new Company(1, "My Company", 500);
        user = mock(User.class);
    }

    @Nested
    class Distribute {

        @DisplayName("Distribute gift card successfully when company balance is sufficient")
        @Test
        void should_distribute_when_company_balance_is_sufficient() throws InsufficientCompanyBalanceException {
            // given
            Distribution distribution = spy(new GiftCardDistribution(1, 499, Clock.systemDefaultZone()));
            // when
            company.distribute(user, distribution);
            // then
            verify(user).receiveDistribution(distribution);
            verify(distribution).linkToCompanyAndUser(company.getId(), user.getId());
            assertEquals(company.getBalance(), 1);
        }

        @DisplayName("Distribute gift card successfully when company balance is equal to amount")
        @Test
        void should_distribute_when_company_balance_is_equal_to_amount() throws InsufficientCompanyBalanceException {
            // given
            Distribution distribution = spy(new GiftCardDistribution(1, 500, Clock.systemDefaultZone()));
            // when
            company.distribute(user, distribution);
            // then
            verify(user).receiveDistribution(distribution);
            verify(distribution).linkToCompanyAndUser(company.getId(), user.getId());
            assertEquals(company.getBalance(), 0);
        }

        @DisplayName("Do not distribute gift card when company balance is not sufficient")
        @Test
        void should_throw_exception_when_company_balance_is_not_sufficient() throws InsufficientCompanyBalanceException {
            // given
            Distribution distribution = new GiftCardDistribution(1, 501, Clock.systemDefaultZone());
            // when
            InsufficientCompanyBalanceException exception = assertThrows(InsufficientCompanyBalanceException.class, () -> company.distribute(user, distribution));
            assertEquals("Company with id 1 has balance of 500. Cannot distribute 501", exception.getMessage());
        }
    }
}
