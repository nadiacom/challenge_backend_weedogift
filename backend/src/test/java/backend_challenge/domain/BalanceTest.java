package backend_challenge.domain;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class BalanceTest {

    @Nested
    class GetBalanceAmount {

        @Test
        void should_sum_all_not_expired_distribution_amounts() {
            // given
            Clock clock = Clock.systemDefaultZone();
            GiftCardDistribution distribution1 = spy(new GiftCardDistribution(1, 50, clock));
            GiftCardDistribution distribution2 = spy(new GiftCardDistribution(2, 150, clock));
            when(distribution1.isExpired(clock)).thenReturn(false);
            when(distribution2.isExpired(clock)).thenReturn(false);

            Balance balance = new Balance(50, List.of(distribution1, distribution2));

            // when
            int amount = balance.getBalanceAmount();

            // then
            assertEquals(amount, 250);
        }

        @Test
        void should_not_sum_expired_distribution_amounts() {
            // given
            Clock clock = Clock.systemDefaultZone();
            GiftCardDistribution distribution1 = spy(new GiftCardDistribution(1, 50, clock));
            GiftCardDistribution distribution2 = spy(new GiftCardDistribution(2, 150, clock));
            when(distribution1.isExpired(clock)).thenReturn(false);
            when(distribution2.isExpired(clock)).thenReturn(true);

            Balance balance = new Balance(50, List.of(distribution1, distribution2));

            // when
            int amount = balance.getBalanceAmount();

            // then
            assertEquals(amount, 100);
        }
    }
}
