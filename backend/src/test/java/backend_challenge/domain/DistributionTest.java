package backend_challenge.domain;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class DistributionTest {

    @Nested
    class isExpired {

        @Test
        void should_return_true_when_current_date_after_end_date() {
            // given
            Clock clockCreation = Clock.fixed(Instant.parse("2019-04-09T00:00:00.00Z"), ZoneId.systemDefault());
            Clock clockNow = Clock.fixed(Instant.parse("2021-04-09T00:00:00.00Z"), ZoneId.systemDefault());
            Distribution distribution = new GiftCardDistribution(1, 20, clockCreation);

            // when
            boolean isExpired = distribution.isExpired(clockNow);

            // then
            assertTrue(isExpired);
        }

        @Test
        void should_return_false_when_current_date_before_end_date() {
            // given
            Clock clockCreation = Clock.fixed(Instant.parse("2021-02-09T00:00:00.00Z"), ZoneId.systemDefault());
            Clock clockNow = Clock.fixed(Instant.parse("2021-04-09T00:00:00.00Z"), ZoneId.systemDefault());
            Distribution distribution = new GiftCardDistribution(1, 20, clockCreation);

            // when
            boolean isExpired = distribution.isExpired(clockNow);

            // then
            assertFalse(isExpired);
        }
    }
}