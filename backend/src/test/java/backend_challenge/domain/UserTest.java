package backend_challenge.domain;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class UserTest {

    @Nested
    class ReceiveDistribution {

        @Test
        void should_dispatch_distribution_by_wallet_type() {
            // given
            Clock clock = Clock.systemDefaultZone();
            GiftCardDistribution distribution1 = spy(new GiftCardDistribution(1, 50, clock));
            GiftCardDistribution distribution2 = spy(new GiftCardDistribution(2, 150, clock));
            FoodDistribution distribution3 = spy(new FoodDistribution(3, 150, clock));
            when(distribution1.isExpired(clock)).thenReturn(false);
            when(distribution2.isExpired(clock)).thenReturn(false);
            when(distribution3.isExpired(clock)).thenReturn(false);

            // when
            // User has initially an amount of 50 gift cards
            User user = new User(1, new HashMap<>(Map.of(WalletType.GIFT, new Balance(50))));
            // User receives 50 + 150 gift cards & 150 food cards
            user.receiveDistribution(distribution1);
            user.receiveDistribution(distribution2);
            user.receiveDistribution(distribution3);

            // then
            assertTrue(user.getWalletBalances().get(WalletType.GIFT).getDistributions()
                    .containsAll(List.of(distribution1, distribution2)));
            assertTrue(user.getWalletBalances().get(WalletType.FOOD).getDistributions()
                    .contains(distribution3));
        }
    }
}
