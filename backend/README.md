# Backend challenge &middot; 
## Level 1 & Level 2
* Main method generates output.json (only dates, indent and order differ from output-expected.json)
* Unit tests

## Level 3
### REST Controllers
* GET /users/{id}/balances
* POST /companies/{companyId}/users/{userId}/distributions
  Body :
```
{
  "wallet_id" : 1,
  "amount : 50
}
```
### Spring Security, jwt & Auth2
#### Authorization
* All end points are authorized. You need valid token to access it.
#### Authentication
* Only GET /users/{id}/balances is authenticated. A user can only access its own balances

#### How to Use it
Authorization : Bearer Token

A jwt secret key is hardcoded is application.properties.
No endpoint to get jwt is implemented, you have to generate it yourself.
To do so, use https://jwt.io/

* Set jwt payload with :
```
{
    ""user_id" : ${USER_ID}
}
```
* Set signature secret (your-256-bit-secret) to value with key "user.jwt-secret" from application.properties

Example of valid token for ${USER_ID} = 1 :
```
eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMSIsImNvbXBhbnlfaWQiOiIxIn0.IAw4k-PnO5WX2SXJwICLlcjMOF8a6zUe1g26BLujqwo
```

## Set up
1. Clone this repository
2. Build it with maven :
```
mvn clean package -DskipTests
```
3. Run Application
```
 java -jar target/backend-1.0.0.jar
```
It runs in localhost:8080 by default. 
